1. Membuat Database

create database myshop;


2. Membuat Table di Dalam Database

A.users
create table users(id int auto_increment, name varchar(255), email varchar(255), password varchar(255), primary key(id));

B.items
create table items(id int auto_increment, name varchar(255), description varchar(255), price int, stock int, category_id int, primary key(id), foreign key(category_id) REFERENCES categories(id));

C.categories
create table categories(id int auto_increment, name varchar(255), primary key(id));

3. Memasukkan Data pada Table

A.users
insert into users Values(null,"John Doe","john@doe.com","john123"),(null,"Jane Doe","jane@doe.com","jenita123");

B.categories
insert into categories Values(null,"gadget"),(null,"cloth"),(null,"men"),(null,"women"),(null,"branded");

C.items
insert into items Values(null,"Sumsang b50","hape keren dari merek sumsang",4000000,100,1),(null,"Uniklooh","baju keren dari brand ternama",500000,50,2),(null,"IMHO Watch","jam tangan anak yang jujur banget",2000000,10,1);


4.Mengambil Data dari Database

A. Mengambil data users

SELECT id,name,email FROM users;

B. Mengambil data items

SELECT * FROM items WHERE price>1000000;
SELECT * FROM items WHERE name LIKE "%sang%";

C. Menampilkan data items join dengan kategori
SELECT items.name, items.description, items.price, items.stock, items.category_id, categories.name FROM items INNER JOIN categories ON items.category_id = categories.id;

5.Mengubah Data dari Database

UPDATE items SET price = 2500000 WHERE name LIKE "%sumsang%";